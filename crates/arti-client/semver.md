ADDED: `vanguards` experimental feature
ADDED: `config::vanguards` module, gated behind `vanguards` feature
ADDED: `vanguards` to `TorClientConfigBuilder`, gated behind `vanguards` feature
ADDED: `HsCircPoolConfig` impl for `TorClientConfig`
